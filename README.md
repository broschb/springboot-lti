This is a very basic sample application that allows itself to be launched as an LTI tool.  The tool provides the following functionality.

1. Authenticates the LTI launch is valid
2. Displays LTI launch parameters on page
3. Provides endpoint for lti xml based configuration in Canvas.

## Technologies used
Maven(https://maven.apache.org/) - used to build and manage dependencies
SpringBoot(https://projects.spring.io/spring-boot/) - Used to simplify creation and running of application.  Provided 'uber' jar that contains all dependencies including webserver to make running application simple.
basiclti-util(https://github.com/IMSGlobal/basiclti-util-java) - Utility library used for LTI launch authentication
Thymeleaf(http://www.thymeleaf.org/) - template language.  This is used to render layout for page.


## Building
run `mvn clean install` to build artifacts.  An already built jar file has been provided under dist

## Running
simply run `java -jar springboot-lti-0.0.1-SNAPSHOT.jar`
This will start a webserver running on port 8080



##### LtiLaunchController.java contains most of the logic relevant to LTI requests.
1. To configure in Canvas under account settings/Apps select + App
2. set the following configurations
    * Configuration Type - By URL
    * Name - Enter name
    * Consumer Key - uniq key for application configuration
    * Shared Secret - this is a shared secret used to auth the request.  In this example app it is hardcoded as `secret`
    * Config Url - this allows the LTI configuration to be read from url, this application publishes xml that can be used at the following path: http://localhost:8080/xml.xml.
3. Make sure server is running and press Submit.

This LTI is configured to be launched from the course, navigate to a course in Canvas and find the tool called `SpringBoot LTI`


## Key Values in LTI launch
* user_id - this will be a uniq string to id the user in the context of an LTI launch.  It will be uniq and consistent for a user, but is different from a Canvas userId.  In order to use this id in an api call you can prepend the id with `lti_context_id:`.  For example to call the users profile api if the token user_id was abc123xz you would call `/api/v1/users/lti_context_id:abc123xyz/profile`
* context_id - this is a uniq identifier for the context in which the lti is launched.  For courses this would be a uniq id for the course.  Similar to users this could be used to make an api call for a course by prepending with `lti_context_id:`.

## What's next
Oauth - you can check the Canvas documentation(https://canvas.instructure.com/doc/api/file.oauth.html) on how to initiate an oauth request.
Api Calls - Once you have established Oauth, you can make api calls to retrieve additional information that may be needed to display.  Documentation can be found here(https://api.instructure.com)





