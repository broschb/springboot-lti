package com.test.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Simple model that just allows for storing a map of values.
 * Shows example of how model could be used, this could be backed
 * by DB model as well.
 *
 */
public class HashModel {
	private Map<String, String> entries = new HashMap<String, String>();
	
	public Map getEntries(){
	    return this.entries;
	}
	public void newEntry(String key, String value){
	    entries.put(key, value);
	}
	
	public List<String> keyValues(){
		List<String> l = new ArrayList<String>();
		for (Map.Entry<String, String> entry : entries.entrySet()) {
			l.add("Key : " + entry.getKey() + " Value : " + entry.getValue());
		}
		return l;
	}

}
